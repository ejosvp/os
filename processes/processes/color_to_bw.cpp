#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/photo/photo.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/contrib/contrib.hpp"
#include "opencv2/core/core_c.h"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/imgproc/imgproc_c.h"
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h> 
#include <sys/shm.h> 
#include <stdio.h>

//0.21 R + 0.72 G + 0.07 B.

#define SHMSZ 8

using namespace cv; 
using namespace std; 

int main(){    
    int shmid;
    key_t key;
    char *shm;

    key = 1337;

    if ((shmid = shmget(key, SHMSZ, 0666)) < 0) {
        perror("shmget");
        return -1;
    }
    if ((shm = (char*)shmat(shmid, NULL, 0)) == (char *) -1) {
        perror("shmat");
        return -1;
    }    

    Mat orig_img = imread("../media/32.jpg", CV_LOAD_IMAGE_UNCHANGED);    
    
    if(orig_img.empty()){
        cout<<"couldn't load image"<<endl;
        return -1;
    }
    namedWindow("Window", CV_WINDOW_NORMAL);    
    
    for (int i = 0; i < orig_img.rows; ++i)
    {
        for (int j = 0; j < orig_img.cols; ++j)
        {

            if(*shm == 1){
                cout<<(int)(*shm)<<endl;
                Vec3b vec = orig_img.at<Vec3b>(i, j);
                //uchar temp = (vec[0]+vec[1]+vec[2])/3;
                uchar temp = vec[0]*0.07 + vec[1]*0.72 + vec[2]*0.21;
                Vec3b newPoint(temp,temp,temp);
                orig_img.at<Vec3b>(i, j) = newPoint;
            }else{
                cout<<(int)(*shm)<<endl;
                sleep(3);
            }

        }
        imshow("Window", orig_img);
        waitKey(1);
    }
    imshow("Window", orig_img);
    waitKey(0);
    destroyWindow("Window");
    return 0;

}
