import time
import sys

PY_MAJOR_VERSION = sys.version_info[0]


def say(s):
    who = sys.argv[0]
    if who.endswith(".py"):
        who = who[:-3]
    s = "%s@%1.6f: %s" % (who, time.time(), s)
    print (s)


def write_to_memory(memory, s):
    say("writing %s " % s)
    s += '\0'
    if PY_MAJOR_VERSION > 2:
        s = s.encode()
    memory.write(s)


def read_from_memory(memory):
    s = memory.read()
    if PY_MAJOR_VERSION > 2:
        s = s.decode()
    i = s.find('\0')
    if i != -1:
        s = s[:i]
    say("read %s" % s)
    return s


def read_params():
    return dict(
        KEY=90,
    )
