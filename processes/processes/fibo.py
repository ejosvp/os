#!/usr/bin/env python


class Matrix(object):
    data = None

    def __init__(self, *args, **kwargs):
        self.data = [[0, 0], [0, 0]]

    def mult(self, x):
        x1 = self.data[0][0] * x.data[0][0] + self.data[0][1] * x.data[1][0]
        x2 = self.data[0][0] * x.data[0][1] + self.data[0][1] * x.data[1][1]
        x3 = self.data[1][0] * x.data[0][0] + self.data[1][1] * x.data[1][0]
        x4 = self.data[1][0] * x.data[0][1] + self.data[1][1] * x.data[1][1]
        self.data[0][0] = x1
        self.data[0][1] = x2
        self.data[1][0] = x3
        self.data[1][1] = x4


def fibo(n):
    m = Matrix()
    m.data[0][0] = 1
    m.data[0][1] = 1
    m.data[1][0] = 1
    acum = Matrix()
    acum.data[0][0] = 1
    acum.data[1][1] = 1
    n -= 2
    while n:
        if not n % 2 == 0:
            acum.mult(m)    # acum = acum * m
        m.mult(m)           # m = m * m
        n /= 2
    return acum.data[0][0] + acum.data[0][1]


if __name__ == "__main__":
    # TODO: print to a file
    print 1
    for i in xrange(2, 1000000):
        print fibo(i)
