#include <sys/types.h>
#include <sys/ipc.h> 
#include <sys/shm.h> 
#include <stdio.h>

#define SHMSZ  27


int main(int argc, char const *argv[])
{
    char c;
    int shmid;
    key_t key;
    char* shm;
    char* s;
    key = 1337;

    if ((shmid = shmget(key, SHMSZ, IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        return;
    }

    if ( (shm = shmat(shmid, NULL, 0)) == ((char *) -1) ) {
        perror("shmat");
        return; 
    }
    printf("Putting value 5 in shared memory\n");
    s = shm;
    *shm = 1;

    while(1){
        sleep(5);
        if(*shm == 1){
            printf("Putting value 0 in shared memory\n");
            *shm = 0;
        }else{
            printf("Putting value 1 in shared memory\n");
            *shm = 1;
        }
    }
    printf("\n shared memory value was changed to :");    
    printf("%i \n", (int)*s);


    return 0;
}