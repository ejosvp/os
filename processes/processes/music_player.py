#!/usr/bin/env python
import sys
import os
import random
import pygame as pg
from pygame.locals import HWSURFACE, HWPALETTE
import sysv_ipc
import utils

what_i_wrote = ""


def build_palette(step):
    """
        Build a palette. that is a list with 256 RGB triplets
    """
    loop = range(256)
    # first we create a 256-element array. it goes from 0, to 255, and back to 0
    ramp = [abs((x + step * 3) % 511 - 255) for x in loop]
    # using the previous ramp and some other crude math, we make some different
    # values for each R, G, and B color planes
    return [(ramp[x], ramp[(x + 32) % 256], (x + step) % 256) for x in loop]


def music_player():
    TRACK_END = pg.USEREVENT + 1
    TRACKS = [
        "../media/track1.ogg",
        "../media/track2.ogg",
    ]
    FPS = 60
    step = 0
    track = 0

    pg.init()

    clock = pg.time.Clock()
    screen = pg.display.set_mode((640, 480), HWSURFACE | HWPALETTE, 8)
    screen.set_palette(build_palette(0))

    pg.mixer.music.set_endevent(TRACK_END)
    pg.mixer.music.load(TRACKS[0])
    pg.display.set_caption('Playing: ' + TRACKS[0])
    pg.mixer.music.play()

    img = pg.image.load('../media/playing.gif')
    screen.blit(img, (0, 0))
    pg.display.flip()

    RUN = True

    while True:
        if RUN:
            screen.set_palette(build_palette(step))
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    pg.quit()
                    sys.exit()
                elif event.type == TRACK_END:
                    track = (track + 1) % len(TRACKS)
                    pg.mixer.music.load(TRACKS[track])
                    pg.display.set_caption('Playing: ' + TRACKS[track])
                    pg.mixer.music.play()
            step += 1
            clock.tick(FPS)
            FPS = random.randint(60, 120)
        s = utils.read_from_memory(memory)
        if s == what_i_wrote:
            if not RUN:
                pg.mixer.music.play()
            RUN = True
        else:
            RUN = False
            pg.mixer.music.pause()

if __name__ == "__main__":
    params = utils.read_params()
    memory = sysv_ipc.SharedMemory(params["KEY"], sysv_ipc.IPC_CREX)
    what_i_wrote = str(os.getpid())
    utils.write_to_memory(memory, what_i_wrote)
    print 'My PID is:', os.getpid()
    print 'My KEY is:', params["KEY"]
    music_player()
