#include <iostream>
#include <chrono>
#include <thread>

using namespace std;

int main() {
    for (int x = 1; x < 100000; x++) {
        for (int i = 1; i < 10; i++) {
            cout << "\t" << i*x % 100;
        }
        this_thread::sleep_for(chrono::milliseconds(200));
        cout << endl;
    }
    return 0;
}
