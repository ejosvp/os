/**
  * Ernesto Jose Vargas Paz
  * ejosvp@gmail.com
  */

#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <fstream>

/**
 * Random numbers using unix urandom
 *
 * @brief urand
 * @param ini
 * @param end
 * @return random number between ini and end
 */
inline uint urand(uint ini = 0, uint end = 0)
{
    std::fstream fs;
    fs.open("/dev/urandom", std::ios::in|std::ios::binary);
    uint a = (uint) fs.get();
    fs.close();

    srand(a);

    if (ini || end) return rand() % (end - ini) + ini;
    return rand();
}


#endif // UTILS_H
