#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <mutex>
#include <sstream>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>

#include "snake.h"

#include <iostream>

#define forever for(;;)

bool wait = 0;
board_type board;
struct Snake;
std::map<int, Snake*> snakes;
std::mutex drawMutex;

int framerate{60};

int shmid;
key_t shmkey = 667788;

void drawingSnake(int id)
{
    snakes[id] = new Snake(id, &board);

    forever
    {
        if (snakes.find(id) == snakes.end()) return;
        drawMutex.lock();
        if (!wait) snakes[id]->move();
        drawMutex.unlock();
        sf::sleep(sf::seconds(1.0 / framerate));
    }
}

int main()
{
    std::cout << "asd";

    sf::RenderWindow window{{windowWidth, windowHeight}, "My Window"};
    window.setFramerateLimit(0);

    // text UI
    sf::Font font;
    if (!font.loadFromFile("font.ttf")) return 1;

    sf::Text sframerate;
    sframerate.setFont(font);
    sframerate.setCharacterSize(24);
    sframerate.setColor(sf::Color::White);
    sframerate.setPosition(windowWidth - 100, 0);
    std::stringstream fps;
    fps << "FPS: " << framerate;
    sframerate.setString(fps.str());

    sf::Text snsnakes;
    snsnakes.setFont(font);
    snsnakes.setCharacterSize(24);
    snsnakes.setColor(sf::Color::White);
    snsnakes.setPosition(10, 0);
    snsnakes.setString("Snakes: 0");

    sf::Text keys;
    keys.setFont(font);
    keys.setCharacterSize(14);
    keys.setColor(sf::Color::White);
    keys.setPosition(10, windowHeight - 20);
    keys.setString("Esc: exit\tP: pause\tR: reset\tUp: add snake\tLeft/Right: change FPS");

    sf::RectangleShape box{sf::Vector2f{nodeSize, nodeSize}};

    std::vector<sf::Thread*> snakeThreads;

//    shmid = shmget(shmkey, sizeof(int), 0666);
//    char* shmpointer = new char;
//    shmpointer = (char*) shmat(shmid, (char*)0, 0);
//    shmdt(&shmpointer);

    while (window.isOpen())
    {
        window.clear();

        sf::Event event;
        while (window.pollEvent(event))
        {
//            if (*shmpointer == 123) sleep(1);

            if (event.type == sf::Event::KeyPressed)
            {
                if (event.key.code == sf::Keyboard::Escape) window.close();
                if (event.key.code == sf::Keyboard::P) wait = !wait;
                if (event.key.code == sf::Keyboard::R)
                {
                    snakes.clear();
                    board.clear();
                }
                if (event.key.code == sf::Keyboard::Up) // Agregar nuevo Snake
                {
                    sf::Thread* t = new sf::Thread(&drawingSnake, snakeThreads.size() + 1);
                    snakeThreads.push_back(t);
                    t->launch();
                }
                if (event.key.code == sf::Keyboard::Left)
                {
                    framerate -= 5;
                    if (framerate <= 0) framerate = 5;
                }
                if (event.key.code == sf::Keyboard::Right)
                {
                    framerate += 5;
                    if (framerate >= 120) framerate = 120;
                }
            }
            if (event.type == sf::Event::Closed) window.close();

            // update text UI
            std::stringstream fps;
            fps << "FPS: " << framerate;
            sframerate.setString(fps.str());
            std::stringstream sn;
            sn << "Snakes: " << snakes.size();
            snsnakes.setString(sn.str());
        }

        drawMutex.try_lock();

        for (auto& snake : snakes)
        {
            for (sf::Vector2u node : snake.second->nodes)
            {
                box.setPosition(sf::Vector2f{node.x * nodeSize, node.y * nodeSize});
                box.setFillColor(sf::Color(
                                      (snake.first * 111 % 197) + 58,   //R
                                      (snake.first * 222 % 197) + 58,   //G
                                      (snake.first * 333 % 197) + 58)); //B

                window.draw(box);
            }
        }
        drawMutex.unlock();

        window.draw(sframerate);
        window.draw(snsnakes);
        window.draw(keys);

        window.display();
        sf::sleep(sf::seconds(1.0 / framerate));
    }

    return 0;
}
