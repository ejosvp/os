/**
  * Ernesto Jose Vargas Paz
  * ejosvp@gmail.com
  */

#ifndef SNAKE_H
#define SNAKE_H

#include <SFML/System.hpp>
#include <map>
#include <vector>
#include <mutex>
#include <sstream>
#include "utils.h"

struct cell { int id; std::mutex mutex; };
typedef std::map<float, std::map<float, cell>> board_type;

constexpr int windowWidth{800}, windowHeight{600};
constexpr uint nodeSize{5};
constexpr int rows{(windowWidth / nodeSize) - 1}, cols{(windowHeight / nodeSize) - 1};
constexpr int snakeLen{25};

struct Snake
{
    board_type* board;

    sf::Vector2u nodes[snakeLen];
    sf::Vector2i dir;
    int id;
    bool dead;

    Snake(int id, board_type* board);
    void move();
    void clear();
};

#endif // SNAKE_H
