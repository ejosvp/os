/**
  * Ernesto Jose Vargas Paz
  * ejosvp@gmail.com
  */

#include "snake.h"

Snake::Snake(int id, board_type* board) : id(id), board(board)
{
    // init dir
    dir.x = urand(0, 2); if (dir.x == 0) dir.x = -1;
    dir.y = urand(0, 2); if (dir.y == 0) dir.y = -1;

    // head
    int x = urand(snakeLen, rows - snakeLen);
    int y = urand(snakeLen, cols - snakeLen);
    (*board)[x][y].mutex.lock();
    (*board)[x][y].id = id;
    nodes[0] = sf::Vector2u{x, y};

    // body
    for (uint i = 1; i < snakeLen; ++i)
    {
        uint x = nodes[i - 1].x - dir.x;
        uint y = nodes[i - 1].y - dir.y;
        nodes[i] = sf::Vector2u{x, y};
        (*board)[nodes[i].x][nodes[i].y].mutex.lock();
        (*board)[nodes[i].x-1][nodes[i].y].mutex.lock();
        (*board)[nodes[i].x][nodes[i].y].id = id;
    }
}

void Snake::clear()
{
    for (sf::Vector2u n : nodes)
    {
        (*board)[n.x][n.y].id = 0;
        (*board)[n.x][n.y].mutex.unlock();
    }
}

void Snake::move()
{
    sf::Vector2i olddir = dir;
    if (urand(0, 20) == 0) {        // 5% de probabilidad de cambiar de direccion
        dir.x = urand(0, 2); if (dir.x == 0) dir.x = -1;
        dir.y = urand(0, 2); if (dir.y == 0) dir.y = -1;

        // verificar que no vaya en reversa
        if (dir.x == -olddir.x) urand(0, 1) ? dir.x = olddir.x : dir.y = olddir.y;
    }

    // evitar bordes de ventana
    if (nodes[0].x == 0) dir.x = 1;
    if (nodes[0].y == 0) dir.y = 1;
    if (nodes[0].x == rows) dir.x = -1;
    if (nodes[0].y == cols) dir.y = -1;

    sf::Vector2u follow = nodes[0];

    // head
    uint x = nodes[0].x + dir.x;
    uint y = nodes[0].y + dir.y;
    (*board)[x][y].mutex.lock();
    (*board)[x-1][y].mutex.lock();
    nodes[0] = sf::Vector2u{x, y};

    // lock tail
    sf::Vector2u tail = nodes[snakeLen-1];

    // body
    for (uint i = 1; i < snakeLen; ++i)
    {
        sf::Vector2u tmp = nodes[i];
        nodes[i].x = follow.x;
        nodes[i].y = follow.y;
        (*board)[follow.x][follow.y].id = id;
        follow = tmp;
    }

    // tail
    (*board)[tail.x][tail.y].id = 0;
    (*board)[tail.x][tail.y].mutex.unlock();
    (*board)[tail.x-1][tail.y].mutex.unlock();
}
