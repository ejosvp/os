#include <iostream>

#ifndef NODEBUG
#define Debug(x) std::cout << "[DEBUG] " << x << std::endl;
#else
#define Debug(x)
#endif
