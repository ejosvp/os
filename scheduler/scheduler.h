#ifndef SCHED_H
#define SCHED_H

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <signal.h>
#include <iostream>
#include <algorithm>    
#include <chrono>
#include <thread>
#include <vector>
#include <queue>
#include <map>
#include <unordered_map>
#include <stdlib.h>
#include <unistd.h>
#include <list>

#include "debug.h"
#include "swap.h"

using namespace std;

#define QUANTUM 10
#define SIGSTATUS 0
#define SHMZ 128
#define MSGSZ 128

#ifndef QUANTUM_MULTILEVEL
#define QUANTUM_MULTILEVEL

#define LEVELS 3
#define QUANTUM_LVL_0 1000
#define QUANTUM_LVL_1 1500
#define QUANTUM_LVL_2 2000

#define PROC_LIMIT 2

const int QUANTUM_LVL[LEVELS] = {
    QUANTUM_LVL_0,
    QUANTUM_LVL_1,
    QUANTUM_LVL_2,
};

#endif // QUANTUM_MULTILEVEL

typedef struct {
    long    mtype;
    pid_t   pid;
} message_buf;

struct SchedulingStrategy {
protected:

    inline bool is_running(int pid) { return kill(pid, SIGSTATUS) == 0; }

    inline void stop_process(int pid) { kill(pid, SIGSTOP); }

    inline void continue_process(int pid) {
        kill(pid, SIGCONT);
        quantum_time_process();
    }

public:
    virtual void next() = 0;
    virtual void quantum_time_process() = 0;
};

struct RoundRobin: public SchedulingStrategy {
    int current;

    void quantum_time_process() {
        this_thread::sleep_for(chrono::milliseconds(QUANTUM));
        Debug("RUNNING " << current);
    }

public:
    RoundRobin(): current(-1) {}

    virtual void next(vector<int> &processes) {
        if (processes.size() > 0) {
            if (current < 0)
                current = 0;
            else {
                stop_process(processes[current]);
                ++current;
                current %= processes.size();
            }
            continue_process(processes[current]);
        }
    }
};

struct proc {
    pid_t pid;
    vector<pid_t> deps;

    pid_t pfind(pid_t id) {
        if (deps.empty()) return 0;
        vector<pid_t>::iterator it = find(deps.begin(), deps.end(), id);
        return *it;
    }

    void premove(pid_t id) {
        vector<pid_t>::iterator it = find(deps.begin(), deps.end(), id);
        deps.erase(it);
    }
};

struct MultiLevel: public SchedulingStrategy {
private:
    int current_pid_idx;
    int current_level;
    int current_pid;

    queue<int> plevels[LEVELS];
    vector<int> blacklist;
    proc root_proc;
    size_t n_proc;
    btree<int>* disk;
    queue<int> q;
public:
    void quantum_time_process() {
        this_thread::sleep_for(chrono::milliseconds(
            QUANTUM_LVL[current_level]
        ));
        Debug("RUNNING " << current_pid << " at LEVEL " << current_level);
    }

    MultiLevel(): current_pid_idx(-1), current_level(0) {
        Debug("init sched")
        disk = new btree<int>("/tmp/Os_proc");
        n_proc = 0;
        current_pid = 0;
    }

    ~MultiLevel() {
        delete [] plevels;
    }

    void add_new(const pid_t &process) {
        if (n_proc == PROC_LIMIT) {
            disk->insert(q.front());
            q.pop();
            root_proc.premove(process);
        }
        //if (root_proc.pfind(process)) return;
        if (is_running(process)) {
            Debug("ADDING " << process);
            stop_process(process);
            plevels[0].push(process);
            q.push(process);
            n_proc++;
        }
    }

    void remove(const pid_t &process) {
        if (root_proc.pfind(process)) {
            if (is_running(process)) {
                Debug("REMOVING " << process);
                stop_process(process);
                blacklist.push_back(process);
                n_proc--;
            }
        }
    }

    virtual void next() {
        if (! plevels[current_level].empty()) {
            current_pid = plevels[current_level].front();
            plevels[current_level].pop();
            if (find(blacklist.begin(), blacklist.end(), current_pid) == blacklist.end())
                return;
            continue_process(current_pid);
            ++current_level;
            if (is_running(current_pid)) {
                stop_process(current_pid);
                if (current_level < LEVELS)
                    plevels[current_level].push(current_pid);
                else
                    plevels[LEVELS - 1].push(current_pid);
            } else {
                Debug("DONE " << current_pid);
            }
        } else {
            Debug("EMPTY LVL " << current_level);
            ++current_level;
        }
        current_level %= LEVELS;
    }

    int run()
    {
        int shmid;        
        char *shm;
        key_t key;
     
        key = 1337;
        
        if ((shmid = shmget(key, SHMSZ, 0666)) < 0) perror("shmget");
    
        if ((shm = shmat(shmid, NULL, 0)) == (char *) -1) perror("shmat");

        for(;;) {
            next();
            if (*shm != 0){
                int c = *shm;
                Debug("proc " << c);
                this->add_new(c);            
            }           
        };
        return 0;
    }
};

#endif // SCHED_H
