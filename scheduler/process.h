#ifndef PROCESS_H
#define PROCESS_H

#include <stdlib.h>
#include <sys/types.h>
#include <vector>
#include <map>

using namespace std;

struct process {
    int id;
    process* parent;
    vector<process> deps;

    process() : id(0) {
    }

    process(pid_t _id) : id(_id) {
    }

    bool operator <(const process &p) { return id < p.id; }

    process* find2(const pid_t &pid, process proc) {
        if (pid == id) return &proc;
        for (process &p : deps)
            find2(pid, p);
        return NULL;
    }
    process* find(pid_t pid) {
        return find2(pid, *this);
    }

    bool delete_child(pid_t pid){
        for (vector<process>::iterator p = deps.begin(); p!= deps.end(); p++)
        {
            if((*p).id == pid){
                deps.erase(p);
                return true;
            }
        }
        return false;
    }

};

#endif // PROCESS_H
