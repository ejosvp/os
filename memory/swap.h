#ifndef SWAP_H
#define SWAP_H

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>

using namespace std;

class pagemanager :protected fstream{
public:
    pagemanager(string file_name)
    : fstream(file_name.data(), ios::in | ios::out | ios::binary )
    {
        empty = false;
        fileName = file_name;
        if(!good()) {
            empty = true;
            open(file_name.data(), ios::in | ios::out | ios::trunc | ios::binary );
        }
    }
    pagemanager() {
        close();
    }
    bool is_empty() {
        return empty;
    }

    template<class Register>
    void save(const long &n ,Register &reg)
    {
        clear();
        seekp(n*sizeof(Register), ios::beg);
        write(reinterpret_cast<const char *> (&reg), sizeof(reg));

    }

    template<class Register>

    long save(Register &reg)
    {
        clear();
        seekp(0, ios::end);
        write(reinterpret_cast<const char *> (&reg), sizeof(reg));

        return page_id_count - 1;
    }

    template<class Register>

    bool recover(const long &n, Register &reg)
    {
        clear();
        seekg(n*sizeof(Register), ios::beg);
        read(reinterpret_cast< char *> (&reg), sizeof(reg));
        return gcount() > 0;
    }

    // Marca el registro como borrado:

    template<class Register>
    void erase(const long &n)
    {
        char mark;
        clear();
        mark = 'N';
        seekg(n*sizeof(Register), ios::beg);
        write(&mark, 1);

    }


private:
    string fileName;
    bool empty;
    long page_id_count;

};

#define m 10
#define OVERFLOW 0
#define OK 1

template <class T>
class btree {

    struct node {
        long page_id;
        int count;
        T data[m + 1];
        long children[m + 2];
        node () {
            count = 0;
            for (int i = 0; i < m+2;i++)
                children [i] = 0;
        }
        void insert_into (int pos, const T& info) {
            int i;
            for (i = m-1; i >= pos; i--) {
                this->data[i+1] = this->data[i];
                this->children[i+2] = this->children[i+1];
            }
            this->children[i+2] = this->children[i+1];
            this->data[pos] = info;
            this->count++;
        }
    };
    node root;
    int header ;
    pagemanager pageman;

public:
    btree (string filename ) : pageman (filename)
    {
        if ( pageman.is_empty ()  ) {
            header = 0;
            root = newNode();
        }
        else {
            pageman.recover (0, header);
            pageman.recover (1, root);
        }
    }

    node newNode () {

        header++;
        pageman.save (0, header);
        node nNode;
        nNode.page_id = header;
        pageman.save (header, nNode);
        return nNode;
    }

    void insert (const T & info) {
        int ret = insert2 (root.page_id, info);
        if (ret == OVERFLOW)
            splitRoot();
    }

    void print () {
            print2 (root.page_id, 0);
    }

private:
    int insert2 (long parent_id, const T& info){
        int pos = 0;
        node parent;
        pageman.recover(parent_id, parent);
        while (pos < parent.count && parent.data[pos] < info)
            pos++;
        if ( parent.children[pos]  == 0) {
            parent.insert_into ( pos, info);
            pageman.save(parent_id, parent);
        }
        else {
            int ret = insert2 (parent.children[pos], info);
            if ( ret  == OVERFLOW ) {
                split (parent_id, pos);
            }
        }
        return (parent.count > m ) ? OVERFLOW : OK;
    }

    void splitRoot() {

        int i ;
        long parent_id = root.page_id;
        int pos = 0;

        node parent;
        pageman.recover(parent_id, parent);
        node child = parent;

        node node1 = newNode();
        node node2 = newNode();

        for ( i = 0; i < m/2; i++ ) {
            node1.data[i] = child.data[i];
            node1.children[i] = child.children[i];
            node1.count++;
        }
        node1.children[i] = child.children[i];

        i++;
        int k;
        for ( k =0 ; i <= m; k++, i++ ) {
            node2.data[k] = child.data[i];
            node2.children[k] = child.children[i];
            node2.count++;
        }
        node2.children[k] = child.children[i];

        parent.count = 0;
        parent.insert_into (pos, child.data[m/2]);

        parent.children[pos] = node1.page_id;
        parent.children[pos+1] = node2.page_id;

        pageman.save( node1.page_id,   node1);
        pageman.save( node2.page_id,   node2);
        pageman.save ( parent_id,  parent);
    }

    void split (long parent_id ,  int pos) {

        int i ;
        node parent;
        pageman.recover(parent_id, parent);
        long child_id = parent.children[pos] ;

        node child;
        pageman.recover(child_id, child);

        node node1 = child;
        node1.count = 0;

        node node2 = newNode();

        for ( i = 0; i < m/2; i++ ) {
            node1.data[i] = child.data[i];
            node1.children[i] = child.children[i];
            node1.count++;
        }
        node1.children[i] = child.children[i];

        i++;
        int k;
        for ( k =0 ; i <= m; k++, i++ ) {
            node2.data[k] = child.data[i];
            node2.children[k] = child.children[i];
            node2.count++;
        }
        node2.children[k] = child.children[i];

        parent.insert_into (pos, child.data[m/2]);

        parent.children[pos] = node1.page_id;
        parent.children[pos+1] = node2.page_id;

        pageman.save( node1.page_id,   node1);
        pageman.save( node2.page_id,   node2);
        pageman.save( parent_id,  parent);
    }

    void print2(long node_id, int level ) {

        if (node_id) {
            node ptr;
            pageman.recover (node_id, ptr);
            int i;
            for (i = ptr.count - 1; i >= 0; i--) {
               print2(ptr.children[i+1], level + 1);

                for (int k = 0; k  < level; k++)
                    cout << "   ";
                cout << ptr.data [i] << endl ;
            }
            print2( ptr.children[i+1], level + 1);

        }
    }
};

#endif // SWAP_H
