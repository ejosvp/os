#ifndef MMU_H
#define MMU_H

#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>
#include <iostream>
#include "swap.h"

#define MEMORY_MAX SHRT_MAX  // 16 bits
#define PID_MAX CHAR_MAX    // 8 bits
#define ADDR_MAX CHAR_MAX   // 8 bits

typedef unsigned long long maddr_t;
typedef char MEM;

class mmu {
    MEM space[MEMORY_MAX];
    bool free[MEMORY_MAX];

public:
    mmu() {
        memset(space, 0, MEMORY_MAX);
        memset(free, 1, MEMORY_MAX);

        std::cout << "MEMORY INIT " << MEMORY_MAX << " bytes" << std::endl;
    }
    MEM& get(const pid_t& pid, const maddr_t& addr) {
        if (pid > PID_MAX || addr > ADDR_MAX) return space[0];
        maddr_t real_addr = (pid << 8) + addr;
        if (free[real_addr]) return space[0];

        return space[real_addr];
    }

    void set(const pid_t &pid, const maddr_t &addr, MEM &data)
    {
        if (pid > PID_MAX || addr > ADDR_MAX) return;
        maddr_t real_addr = (pid << 8) + addr;
        free[real_addr] = 0;
        space[real_addr] = data;
    }

    bool if_full()
    {
        for (int i = 0; i < MEMORY_MAX; i++)
            if (!free[i]) return false;
        return 1;
    }
};

mmu *manager = new mmu;

extern void _mem_set(const pid_t &pid, const maddr_t &addr, const MEM data)
{
    char buf = data;
    manager->set(pid, addr, buf);
}

extern MEM& _mem_get(const pid_t &pid, const maddr_t &addr)
{
    return manager->get(pid, addr);
}

///// SWAP



#endif // MMU_H
