#ifndef TERM_H
#define TERM_H

#include <unistd.h>
#include <iostream>
#include <vector>
#include <fstream>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>

#define MSGSZ 128
#define SHMSZ 128

typedef struct {
    long    mtype;
    pid_t   pid;
} message_buf;

using namespace std;

pid_t exec(const char * command)
{
    int p_stdin[2];
    int p_stdout[2];
    pid_t pid;

    if (pipe(p_stdin) == -1)
        return -1;

    if (pipe(p_stdout) == -1) {
        close(p_stdin[0]);
        close(p_stdin[1]);
        return -1;
    }

    pid = fork();

    if (pid < 0) {
        close(p_stdin[0]);
        close(p_stdin[1]);
        close(p_stdout[0]);
        close(p_stdout[1]);
        return pid;
    } else if (pid == 0) {
        close(p_stdin[1]);
        dup2(p_stdin[0], 0);
        close(p_stdout[0]);
        dup2(p_stdout[1], 1);
        dup2(::open("/dev/null", O_RDONLY), 2);
        /// Close all other descriptors for the safety sake.
        for (int i = 3; i < 4096; ++i)
            ::close(i);

        setsid();
        execl("/bin/sh", "sh", "-c", command, NULL);
        _exit(1);
    }

    close(p_stdin[0]);
    close(p_stdout[1]);

    return pid;
}

class launcher
{
public:
    launcher() {
    }

    int run() {
        string file;

        /*int msgid;
        int msgflg = IPC_CREAT|0666;*/
        int shmid;
        char *shm;
        key_t key;
        key = 1337;        
        if ((shmid = shmget(key, SHMSZ, IPC_CREAT | 0666)) < 0) perror("shmget");    
        if ((shm = shmat(shmid, NULL, 0)) == (char *) -1) perror("shmat");     
        
        pid_t pid;

        for (;;) {
            cout << " > ";
            getline(cin, file);
            if (!file.empty()) {
                if (file == "exit") break;
                pid = exec(file.c_str());

                *shm = pid;
                cout << "LAUNCHED " << file << " " << pid << endl;
            }
        }
        return 0;
    }

};

#endif // TERM_H
